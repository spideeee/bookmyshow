import {FETCH_DATA, LANGUAGE_UPDATE} from "../actionCreators";

let initialState = {
    loading: true,
    languages: [],
    movies: [],
    selectedLanguages: []
};

function mainStore(state = initialState, { type, payload }) {
    let movies;
    switch (type) {
        case FETCH_DATA:
            movies = [];
            console.log(payload);
            Object.keys(payload[1]).forEach((key) => {
                let m = payload[1][key];
                if(m.ShowDate){
                    let a = m.ShowDate.split(",")[0].split(" ");
                    m.releaseDay = a[0];
                    m.releaseMonth = a[1];
                }
                movies.push(m)
                // console.log(m.ShowDate)
            });
            return {
                ...state,
                loading: false,
                movies: movies,
                languages: payload[0]
            }
        break;
        case LANGUAGE_UPDATE:
            return {
                ...state,
                selectedLanguages: payload
            }
        break;
        default:
            return state;
    }
}

export default mainStore;