import React from 'react';
import className from "classnames";

// export default function MovieCard({onClick, data: {EventCode, releaseDay, releaseMonth, wtsPerc, wtsCount, EventName}}){
//     return (
//         <div className="movie-card">
//             <div className="thumbnail">
//                 <img src={"https://in.bmscdn.com/events/moviecard/" + EventCode + ".jpg"} />
//                 <div className="release-info">
//                     {releaseDay}
//                     <br />
//                     {releaseMonth}
//                 </div>
//                 <div className="rating-info">
//                     <i className="fa fa-thumbs-up" aria-hidden="true"></i> {wtsPerc}%
//                     <br />
//                     <span className="votes">{wtsCount} votes</span>
//                 </div>
//                 <div className="play-button">

//                 </div>
//             </div>
//             <div className="movie-title">
//                 {EventName}
//             </div>
//         </div>
//     )
// }

class MovieCard extends React.Component{
    static defaultProps = {
        onClick: () => null
    }
    constructor(props){
        super(props);
        this.onClick = this.onClick.bind(this);
    }
    onClick(e){
        this.props.onClick(e.currentTarget, this.props.data);
    }
    render(){
        let {data: {EventCode, releaseDay, releaseMonth, wtsPerc, wtsCount, EventName}} = this.props;

        return (
            <div className="movie-card" onClick={this.onClick}>
                <div className="thumbnail">
                    <img src={"https://in.bmscdn.com/events/moviecard/" + EventCode + ".jpg"} />
                    <div className="release-info">
                        {releaseDay}
                        <br />
                        {releaseMonth}
                    </div>
                    <div className="rating-info">
                        <i className="fa fa-thumbs-up" aria-hidden="true"></i> {wtsPerc}%
                        <br />
                        <span className="votes">{wtsCount} votes</span>
                    </div>
                    <div className="play-button">
    
                    </div>
                </div>
                <div className="movie-title">
                    {EventName}
                </div>
            </div>
        )
    }
}

export default MovieCard;