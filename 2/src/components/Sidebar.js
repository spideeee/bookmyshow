import React from "react";
import classnames from "classnames";
import {withRouter} from "react-router-dom"

class Sidebar extends React.Component{
    componentDidMount(){
        this.props.history.replace("/conversations");
    }
    render(){
        return (
            <div className="app-sidebar">
                <UserIcon />
                <ul className="options">
                    <li>
                        <FAIcon type="comment"/>
                    </li>
                    <li>
                        <FAIcon type="address-book"/>
                    </li>
                    <li>
                        <FAIcon type="cog" theme="fa"/>
                    </li>
                </ul>
            </div>
        )
    }
}

export default withRouter(Sidebar);

export function UserIcon(){
    return (
        <div className="user-icon">
            <img alt="user-pic" src="/user.png" />
        </div>
    )
}

export function FAIcon(props){
    const { type = "plus", style = {}, theme = "far" } = props;
    let className = classnames(theme, "fa-" + type, props.className);
    return (
        <i className={className} style={style}></i>
    )
}