import React from 'react';
import className from "classnames";

export default function Button({active = false, children, ...props}){
    return (
        <button className={className("button", {active})} {...props}>{children}</button>
    )
}

export function SubmitButton({type = "primary", text = "Submit", ...props}){
    return (
        <input type="submit" value={text} className={className(type, "button")} {...props} />
    )
}

Button.Submit = SubmitButton;