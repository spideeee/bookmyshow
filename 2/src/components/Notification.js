import React from "react";
import Notification from "rc-notification";
import className from "classnames";

let notification = null;
const notificationClassName = "notification-toast";

Notification.newInstance({
    style:{
        right: -180,
        top: 20
    },
    prefixCls: notificationClassName,
    closeIcon: <i className="fa fa-times" />,
    maxCount: 5 // since we have 5 types of notifications
}, (_notification) => {
    notification = _notification;
});

let activeNotices = {};

function notify({title = "", message = "", duration = 2, closable = true, type = "", iconType = "bell", ...restProps} = {}){
    let noticeKey = Math.random() + type + Math.random();
    activeNotices[noticeKey] = noticeKey;

    notification.notice({
        key: noticeKey,
        prefixCls: className(type, notificationClassName),
        content: [
            <i key={"icon"} className={"fa fa-" + iconType} aria-hidden="true"></i>,
            <div key={"content"} className="notification-container">
                <div className="title">
                    {title}
                </div>
                <div className="message">
                    {message}
                </div>
            </div>
        ],
        duration,
        closable,
        ...restProps,
        onClose: (...args) => {
            delete activeNotices[noticeKey];
            if(typeof restProps.onClose === "function"){
                restProps.onClose(...args);
            }
        }
    });

    return function(){
        if(activeNotices[noticeKey]){
            delete activeNotices[noticeKey];
            notification.removeNotice(noticeKey);
        }
    }
}

const module = {
    notify,
    removeAll: () => {
        let keys = Object.keys(activeNotices);
        if(keys.length > 0){
            activeNotices = {};
            keys.forEach(key => notification.removeNotice(key));
        }
    }
};

let iconTypes = {
    warn: "exclamation-triangle",
    info: "info-circle",
    success: "check-circle",
    error: "times-circle fas"
};
["info", "warn", "success", "error"].forEach(type => {
    module[type] = (props = {}) => {
        props.type = type;
        props.iconType = iconTypes[type];
        return notify(props);
    }
});

export default module;