import React from "react";
import { FAIcon } from "./Sidebar";
import 'emoji-mart/css/emoji-mart.css';
import { Picker, Emoji, emojiIndex } from 'emoji-mart';
import Trigger from "rc-trigger";
import 'rc-trigger/assets/index.css';

export default class ConversationInputBox extends React.Component{
    state = {
        text: " "
    }
    text = ""
    selectionRange = {}
    constructor(props){
        super(props);
        this.onEmojiSelect = this.onEmojiSelect.bind(this);
        this.inputChanged = this.inputChanged.bind(this);
    }
    onEmojiSelect(e){
        let {startContainer = this.input, startOffset = 0} = this.selectionRange;
        if(startContainer.insertData){
            startContainer.insertData(startOffset, e.native);
        } else if(startContainer.tagName === "DIV"){
            console.info("Appending to html")
            if(startContainer.innerHTML === "<br>"){
                startContainer.innerHTML = e.native;
            } else {
                startContainer.innerHTML += e.native;
            }
        } else {
            console.warn("Unable to add Emoji: " + e.native + " at " + startOffset, startContainer);
        }
    }
    inputChanged(e){
        this.selectionRange = window.getSelection().getRangeAt(0);
    }
    componentDidUpdate(){

    }
    render(){
        return (
            <div className="conversation-input-box">
                <FAIcon type="plus" theme="fa" />
                <div 
                    className="input" 
                    contentEditable
                    onKeyUp={this.inputChanged}
                    onClick={this.inputChanged}
                    ref={e => this.input = e}
                >
                </div>
                <Emojis 
                    onSelect={this.onEmojiSelect}
                /> 
            </div>
        )
    }
}

function Emojis({onSelect}){
    let emojiPicker = (
        <Picker 
            set='emojione'
            onSelect={onSelect}
            native
        />
    );
    return (
        <Trigger 
            popup={emojiPicker} 
            action={["click"]} 
            popupAlign={{
                points: ['br', 'tr'],
                offset: [6, -14]
            }}
        >
            <div>
                <FAIcon type="smile" theme="far" />
            </div>
        </Trigger>
    )
}