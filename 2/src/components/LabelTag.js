import React from 'react';
import className from "classnames";

class LabelTag extends React.Component{
    static defaultProps = {
        label: "label",
        onClose: () => {},
        closable: true
    }
    constructor(props){
        super(props);
        this.onClose = this.onClose.bind(this);
    }
    onClose(){
        this.props.onClose(this.props.label);
    }
    render(){
        let {closable, id, label} = this.props;
        if(id == undefined || id == null){
            id = label;
        }
        return (
            <span className="label-tag">
                {label}
                {
                    closable
                    ?
                    <span className="close" onClick={this.onClose}>×</span>
                    :
                    null
                }
            </span>
        )
    }
}

export default LabelTag;