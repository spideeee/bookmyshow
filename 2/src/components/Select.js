import React from "react";
import className from "classnames";
import Trigger from "rc-trigger";
import 'rc-trigger/assets/index.css';

export default class Select extends React.Component{
    state = {
        visible: false,
        selected: []
    }
    static defaultProps = {
        onSelect: () => {},
        selected: [],
        type: "selected"
    }
    static getDerivedStateFromProps(props, state){
        if(state.selected.toString() != props.selected.toString()){
            return {
                selected: props.selected
            }
        }
        return null;
    }
    constructor(props){
        super(props);
        this.onOptionClick = this.onOptionClick.bind(this);
        this.onPopupVisibleChange = this.onPopupVisibleChange.bind(this);
        this.ref = React.createRef();
    }
    onOptionClick(e){
        let item = e.currentTarget.dataset.text;
        let selected = this.state.selected.slice(0);
        let index = selected.indexOf(item);
        if(index === -1){
            selected.push(item);
        } else {
            selected.splice(index, 1);
        }
        this.props.onSelect(selected);

    }
    onPopupVisibleChange(visible) {
        console.log(visible)
        this.setState({visible});
    }
    renderOptions(list = this.props.options, selected = this.state.selected){
        if(!selected){
            selected = [];
        }
        let options = list.map((item) => {
            let isSelected = selected.includes(item);
            return (
                <div 
                    className="option"
                    key={item}
                    data-text={item}
                    onClick={this.onOptionClick}
                >
                    <i className={className({"far fa-square": !isSelected, "fas fa-check-square": isSelected})} />
                    &nbsp;&nbsp;
                    {item}
                </div>
            )
        });

        return (
            <div className="select-options"  style={{width: this.props.width}}>
                {options}
            </div>
        )
    }
    getSelectionName(){
        let {options, type} = this.props;
        let {selected} = this.state;
        if(selected.length === options.length){
            return "All " + type;
        }
        if(selected.length === 0){
            return type;
        }
        let text = selected[0];
        if(selected.length == 2){
            text += ", " + selected[1];
        } else if(selected.length > 2){
            text += ", " + (selected.length - 1) + " more";
        }
        return text;
    }
    render(){
        const {width} = this.props;
        const {visible} = this.state;
        let options = this.renderOptions();
        return (
            <Trigger 
                popup={options} 
                action={["click"]} 
                onPopupVisibleChange={this.onPopupVisibleChange}
                afterPopupVisibleChange={this.afterPopupVisibleChange}
                popupVisible={visible}
                popupAlign={this.props.popupAlign ? this.props.popupAlign : {
                    points: ['tl', 'bl'],
                    offset: [0, 3]
                }}
                getPopupContainer={() => this.ref.current}
            >
                <div className="select-container" style={{width}} ref={this.ref}>
                    <div className="select-box">
                        <div className="indicator">{this.getSelectionName()}</div>
                        <div className="arrow">
                            <i className="fa fa-chevron-down"/>
                        </div>
                    </div>
                </div>
            </Trigger>
        )
    }
}