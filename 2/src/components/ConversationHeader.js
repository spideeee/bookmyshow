import React from "react";
import { UserIcon } from "./Sidebar";

export default function ConversationHeader(){
    return (
        <div className="conversation-header">
            <UserInfo />
            <UserActions />
        </div>
    )
}

function UserInfo(){

    return (
        <div className="header-user-info">
            <UserIcon />
            <div className="user-name-message-box">
                <div className="user-name">FirstName LastName</div>
                <div className="last-message">message</div>
            </div>
        </div>
    )
}

function UserActions(){
    return null;
}