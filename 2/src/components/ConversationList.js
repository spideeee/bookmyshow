import React from "react";
import { FAIcon, UserIcon } from "./Sidebar";

class ConversationList extends React.Component{
    render(){
        return (
            <div className="conversation-list">
                <SearchBox />
                {
                    new Array(200).fill(1).map((value, i) => <Conversation key={i} />)
                }
            </div>   
        )
    }
}

export default ConversationList;

function SearchBox(props){

    return (
        <div className="search-box">
            <input type="text" className="search-input" placeholder="Search here..." />
            <FAIcon type="search" theme="fa" />
        </div>
    )
}

function Conversation(props){

    return (
        <div className="conversation">
            <UserIcon />
            <div className="user-name-message-box">
                <div className="user-name">FirstName LastName</div>
                <div className="last-message">message</div>
            </div>
            <div className="user-status-box">
                <div className="last-message-time">Jul 22</div>
                <div className="status online"></div>
            </div>
        </div>
    )
}