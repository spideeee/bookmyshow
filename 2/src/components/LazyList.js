import React from "react";
import className from "classnames";

class LazyList extends React.Component{
    static defaultProps = {
        batchSize: 8,
        Component: () => null,
        data: []
    }
    state = {
        length: 12
    }
    ref = null
    constructor(props){
        super(props);

        this.onWindowScroll = this.onWindowScroll.bind(this);
        this.ref = React.createRef();
    }
    componentDidMount(){
        window.addEventListener("scroll", this.onWindowScroll);
        this.onWindowScroll();
    }
    onWindowScroll(){
        if(this.ref.current.offsetHeight - window.scrollY <= window.screen.availHeight){
            if(this.state.length < this.props.children.length){
                this.setState({
                    length: this.state.length + this.props.batchSize
                }, this.onWindowScroll);
            } else {
                // all children rendered
                window.removeEventListener("scroll", this.onWindowScroll);
            }
        }
    }
    renderList(){
        let {length} = this.state;
        let {data, Component} = this.props;
        let list = [];
        for(let i = 0; i < length && data[i]; i++){
            list.push(
                this.props.children[i]
            )
        }
        return list;
    }
    render(){
        let {className: classes} = this.props;
        return (
            <div className={className("lazy-list", classes)} ref={this.ref}>
                {
                    this.renderList()
                }
            </div>
        )
    }
}

export default LazyList;