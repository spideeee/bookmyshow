import React from "react";
import {Redirect, withRouter} from "react-router-dom";
import MainLoader from "../components/MainLoader";

class Authenticator extends React.Component{
    state = {
        loading: true,
        isAuthenticated: null
    }
    componentDidMount(){
        // this.
        setTimeout(() => {
            this.setState({
                loading: false,
                isAuthenticated: false
            });
            // this.props.history.replace("/login");
        }, 2000);
    }
    render(){
        let { loading, isAuthenticated } = this.state;

        return (
            loading
            ?
            <MainLoader />
            :
            isAuthenticated === false
            ?
            <Redirect to="login" />
            :
            null
        )
    }
}


export default withRouter(Authenticator);