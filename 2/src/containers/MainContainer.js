import React from "react";
import MovieCard from "../components/MovieCard";
import MainLoader from "../components/MainLoader";
import { connect } from "react-redux";
import LazyList from "../components/LazyList";

class MainContainer extends React.Component{
    static defaultProps = {
        selectedLanguages: []
    }
    state = {
        selectedLanguages: [],
        filteredMovies: []
    }
    static getDerivedStateFromProps(props, state){
        if(props.selectedLanguages.length != 0 && state.selectedLanguages.toString() !== props.selectedLanguages.toString()){
            return {
                filteredMovies: props.movies.filter((m) => {
                    return props.selectedLanguages.indexOf(m.EventLanguage) !== -1;
                }),
                selectedLanguages: props.selectedLanguages
            }
        } else {
            return {
                filteredMovies: props.movies,
                selectedLanguages: props.selectedLanguages
            }
        }
        return null;
    }
    constructor(props){
        super(props);
        this.showTrailer = this.showTrailer.bind(this);
    }
    showTrailer(element, movie){
        // console.log(element, movie);
        let parentNode = element.parentElement;
        let allSiblings = parentNode.querySelectorAll("." + element.className);
        let previousSiblings = [], i = 0;
        for(; i < allSiblings.length; i++){
            previousSiblings.push(allSiblings[i]);
            if(allSiblings[i] == element){
                break;
            }
        }
        console.log(previousSiblings.length - 1);
        let currentY = element.getBoundingClientRect().y;
        let trailerIndex = 0;
        for(i = previousSiblings.length - 1; i >= 0; i--){
            if(previousSiblings[i].getBoundingClientRect().y < currentY){
                trailerIndex = i + 1; // index starts from 0 and next to the index found
                break;
            }
        }
        
        // this.setState({
        //     trailerIndex: trailerIndex
        // });
        console.log("Insert trailer before " + (trailerIndex + 1));
    }    
    render(){
        const {loading, movies, selectedLanguages} = this.props;
        const {filteredMovies} = this.state;
        // let filteredMovies = movies;
        // if(selectedLanguages.length > 0){
        //     filteredMovies = movies.filter((m) => {
        //         return selectedLanguages.indexOf(m.EventLanguage) !== -1;
        //     });
        // }

        return (
            <div className="main-container">
                {
                    loading
                    ?
                    <MainLoader />
                    :
                    // <TrailerList movies={filteredMovies} />
                    <LazyList className="trailer-list" data={filteredMovies} Component={MovieCard}>
                        {
                            filteredMovies.map(m => {
                                return <MovieCard data={m} key={m.EventCode} onClick={this.showTrailer}/>
                            })
                        }
                    </LazyList>
                }
                
                {/* <TrailerList /> */}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.loading,
        movies: state.movies,
        selectedLanguages: state.selectedLanguages
    }
}

export default connect(mapStateToProps)(MainContainer);

function TrailerList({movies}){
    return (
        <div className="trailer-list">
            {/* <MovieCard />
            <MovieCard />
            <MovieCard />
            <MovieCard />
            <MovieCard />
            <MovieCard />
            <MovieCard />
            <MovieCard />
            <MovieCard />
            <MovieCard />
            <MovieCard />
            <MovieCard /> */}
            {
                movies.map(m => {
                    return <MovieCard data={m} key={m.EventCode} />
                })
            }
        </div>
    )
}
