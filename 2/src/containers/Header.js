import React from "react";
import { connect } from "react-redux";
import Button from "../components/Button";
import Select from "../components/Select";
import { bindActionCreators } from "redux";
import { languageUpdate } from "../actionCreators";

class Header extends React.Component{
    constructor(props){
        super(props);
        this.onLanguageUpdate = this.onLanguageUpdate.bind(this);
    }
    onLanguageUpdate(languages){
        this.props.languageUpdate(languages);
    }
    render(){
        const {languages, selectedLanguages} = this.props;
        return (
            <div className="app-header">
                <div className="left-container">
                    <div className="title">Movie Trailers</div>
                    <Button active>COMING SOON</Button>
                    <Button>NOW SHOWING</Button>
                </div>
                <div className="right-container">
                    <Select 
                        options={languages} 
                        selected={selectedLanguages} 
                        onSelect={this.onLanguageUpdate} 
                        type="Languages" width={"150px"} 
                        listPositionType="fixed"
                    />
                    {/* <Button>NOW SHOWING</Button> */}
                    <div className="clear-filter">×</div>
                </div>
            </div>
        )
    }
}

const mapStateToPtops = (state) => {
    return {
        languages: state.languages,
        selectedLanguages: state.selectedLanguages
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        languageUpdate
    }, dispatch);
}

export default connect(mapStateToPtops, mapDispatchToProps)(Header); 

