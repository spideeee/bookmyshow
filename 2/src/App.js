import React, { Component } from 'react';
import { HashRouter, Route } from "react-router-dom";
// import Sidebar from './components/Sidebar';
// import Conversations from './containers/Conversations';
// import Authenticator from './containers/Authenticator';
import Header from './containers/Header';
import MainContainer from './containers/MainContainer';
// import './App.css';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {fetchDataAsync, languageUpdate} from "./actionCreators";
import LabelTag from "./components/LabelTag";

class App extends Component {
  static defaultProps = {
    selectedLanguages: []
  }
  constructor(props){
    super(props);
    this.onLanguageFilterClose = this.onLanguageFilterClose.bind(this);
  }

  onLanguageFilterClose(e){
    let language = e;
    let selectedLanguages = this.props.selectedLanguages.slice(0);
    let index = selectedLanguages.indexOf(language);
    if(index !== -1){
      selectedLanguages.splice(index, 1);
      this.props.languageUpdate(selectedLanguages);
    }
  }
  componentDidMount(){
    this.props.fetchDataAsync();
  }
  render() {
    let {selectedLanguages} = this.props;
    return (
      // <HashRouter>
        <div className="App">
          <Header />
          <FilterDetails selectedLanguages={selectedLanguages} onLanguageFilterClose={this.onLanguageFilterClose}/>
          <MainContainer />
          {/* <Route exact path="/conversations" component={Conversations} /> */}
          {/* <Authenticator /> */}
          {/* <Route exact path="/" component={Authenticator} />
          <Route exact path="/login" component={Login} /> */}
        </div>
      // </HashRouter>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectedLanguages: state.selectedLanguages
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchDataAsync,
    languageUpdate
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);


function FilterDetails({selectedLanguages = [], onLanguageFilterClose}){
  return (
      <div className="filter-details">
      {
        selectedLanguages.length == 0
        ?
        null
        :
        <>
          Applied filters: 
          {
            selectedLanguages.map((value, i) => <LabelTag label={value} key={value + i} onClose={onLanguageFilterClose} />)
            
          }
        </>
      }
      </div>
  )
}
