import axios from "axios";
import {DATA_URL} from "../constants/Urls";

export const FETCH_DATA = "FETCH_DATA";

export function fetchData(data){
    return {
        type: FETCH_DATA,
        payload: data
    }
}

export function fetchDataAsync(){
    return dispatch => {
        axios.get(DATA_URL).then(response => {
            // console.log(response.data)
            dispatch(fetchData(response.data));
        }).catch(err => {

        });
    }
}

export const LANGUAGE_UPDATE = "LANGUAGE_UPDATE";

export function languageUpdate(languages){
    return {
        type: LANGUAGE_UPDATE,
        payload: languages
    }
}