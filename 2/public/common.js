/**
 * 
 * @param {String} method 
 * @param {String} url 
 * @param {Object} params 
 * @param {function} callback 
 * @param {function} error 
 */
function request(method, url, params, callback, error){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            if(this.status == 200 || this.status === 202){
                callback(xhttp.response);
            } else {
                error(xhttp.response)
            }
        }
        console.log(this.readyState, this.status);
    };
    xhttp.open(method || "GET", url, true);
    xhttp.setRequestHeader("content-type", "application/json");
    xhttp.send(params ? JSON.stringify(params) : undefined);
}

function parseQuery() {
	var str, array, object = {};
	if (window.location.search.length > 0) {
		str = window.location.search.substr(1, window.location.search.length);
		array = str.split("&");
		array.forEach(function (val) {
			var key = val.split("=")[0];
			var value = val.split("=")[1];
			object[key] = value != "" ? value : true;
		});
	}
	return object;
}
