(function(){
    var tokenId = parseQuery().tokenId;
    request("POST", "/api/activation/" + tokenId, null, function(e){
        console.log(e);
        var json = JSON.parse(e);
        document.body.innerHTML = json.message + " <br/> You will be redirected to login in 5 sec";
        setTimeout(function(){
            window.location.href = "/login";
        }, 5000);
    }, function(e){
        var json = JSON.parse(e);
        document.body.innerHTML = json.message;
    });
})();